# README #

### What is this repository for? ###

This repository is built for an event management website, called "Event Bridge", which provides a platform to facilitate the communication between event organizers and potential participants. 

This is the first version with essential features including post and edit events, book and follow events, as well as comment on events attended and a few admin features like validate users and events.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

This is a project developed using Spring Tool Suit (STS). Therefore, import the project into STS would finish the setup.

The project uses Maven to automatically manage dependencies.

The project uses local database mysql as the backend. Please configure the persistence-mysql.properties file with your own username and password. 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Repo owner